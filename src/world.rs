extern crate rand;
use std;
pub struct Map {
    size: u32,
    light: f32,
    pub wall_grid: [u32;10*10],
}
impl Map{
    pub fn new(size:u32) -> Map {
        Map{
            size:size,
            wall_grid:randomize(10),
            light:0.0,
        }
    }
    pub fn cast(self, x:f32,y:f32,angle:f32,range:f32) {
        let sin = angle.sin();
        let cos = angle.cos();
        let noWall = -(std::f32::INFINITY);
        // return ray(x, y, 0, 0);
    }
}
pub fn randomize(size:u32) -> [u32; 100]{
    let mut grid = [0;10*10];
    for i in 0..grid.len() {
        let random=rand::random::<f32>();
        grid[i] = if random < 0.3 {1} else {0};
    }
    return grid;
}
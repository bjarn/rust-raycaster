use sdl2::render::Canvas;
use sdl2::video::Window;
use sdl2::pixels::Color;
use sdl2::rect::Rect;

use world::Map;

pub struct Camera{
    pub canvas:Canvas<Window>,
    pub width:f32,
    pub height:f32,
    resolution: f32,
    focal_length: f32,
    range: u32,
    scale: f32,
    light_range : u32,
}
impl Camera{
    pub fn new(window: Window, width:f32, height:f32, resolution:f32, focal_length:f32) -> Camera {
        let canvas = init_canvas(window);
        Camera{
            canvas:canvas,
            width:width,
            height:height,
            resolution: resolution,
            focal_length: focal_length,
            range: 14,
            scale: (width + height) / 1200.0,
            light_range : 5,
        }
    }
    pub fn update(mut self, draw_list:Rect){
        self.canvas.set_draw_color(Color::RGB(255, 255, 255));
        self.canvas.draw_rect(draw_list);
        self.canvas.present();
    }
}
//Make the default canvas
fn init_canvas(window:Window) -> Canvas<Window>{
    let mut canvas = window.into_canvas().build().unwrap();
    canvas.set_draw_color(Color::RGB(0, 0, 0));
    canvas.clear();
    canvas.present();
    return canvas;
}